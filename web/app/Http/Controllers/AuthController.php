<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.form');
    }

    public function kirim(Request $request)
    {
        $namadepan=$request['fname'];
        $namabelakang =$request['lname'];

        return view('halaman.welcome', compact('namadepan', 'namabelakang'));
    }
}
