<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=h2, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h4>Sign Up Form</h4>
<form action="/kirim" method="post">
    @csrf
    <label>First name</label> <br> <br>
    <input type="text" name="fname"> <br> <br>
    <label>Last name</label> <br> <br>
    <input type="text" name="lname"> <br> <br>
    <label>Gender:</label> <br> <br>
    <input type="radio" name="" id="1"> Male <br>
    <input type="radio" name="" id="2"> Female <br>
    <input type="radio" name="" id="3"> Other <br> <br>
    <label>Nationality:</label> <br> <br>
    <select name="nationality" id="">
        <option value="1">Indonesian</option>
        <option value="2">Australia</option>
        <option value="3">Singapore</option>
        <option value="4">Malaysia</option>
    </select> <br> <br>
    <label>Language Spoken:</label> <br> <br>
    <input type="checkbox" name="Language"> Bahasa Indonesia <br> <br>
    <input type="checkbox" name="Language"> English <br> <br>
    <input type="checkbox" name="Language"> Other <br> <br>
    <label>Bio:</label> <br> <br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea> <br> <br>
    <input type="submit" value="kirim">
</form>
</body>
</html>
