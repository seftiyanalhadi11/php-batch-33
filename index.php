<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

    echo "nama hewan  : $sheep->name  <br>"; // "shaun"
    echo "jumlah kaki : $sheep->legs  <br>"; // 4
    echo "cold blooded : $sheep->cold_blooded <br> <br>"; // "no"

$sungokong = new Ape("kera sakti");
    echo "nama hewan  : $sungokong->name  <br>";
    echo "jumlah kaki  : $sungokong->legs  <br>";
    echo "cold blooded  : $sungokong->cold_blooded  <br> <br>";
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");

    echo "nama hewan  : $kodok->name  <br>"; // "shaun"
    echo "jumlah kaki : $kodok->legs  <br>"; // 4
    echo "cold blooded : $kodok->cold_blooded <br> <br>";

$kodok->jump() ; // "hop hop"

?>